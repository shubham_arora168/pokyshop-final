<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table {$this->getTable('diva')} (diva_id int not null auto_increment,  
						first_name varchar(20), 
                        middle_name varchar(20), 
                        last_name varchar(20), 
                        mobile_number varchar(10),
                        email varchar(30),
                        dob varchar(20), 
                        age int(2),
                        address varchar(50), 
                        birth_city varchar(50), 
                        height varchar(50), 
                        weight int(2),
                        bust_size int(2),
                        waist_size int(2),
                        hips_size int(2),
                        married varchar(50),
                        passport varchar(20),
                        primary key(diva_id)
                     );
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 