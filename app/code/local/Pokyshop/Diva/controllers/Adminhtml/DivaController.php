<?php

class Pokyshop_Diva_Adminhtml_DivaController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("diva/diva")->_addBreadcrumb(Mage::helper("adminhtml")->__("Diva  Manager"),Mage::helper("adminhtml")->__("Diva Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Diva"));
			    $this->_title($this->__("Manager Diva"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Diva"));
				$this->_title($this->__("Diva"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("diva/diva")->load($id);
				if ($model->getId()) {
					Mage::register("diva_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("diva/diva");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Diva Manager"), Mage::helper("adminhtml")->__("Diva Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Diva Description"), Mage::helper("adminhtml")->__("Diva Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("diva/adminhtml_diva_edit"))->_addLeft($this->getLayout()->createBlock("diva/adminhtml_diva_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("diva")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Diva"));
		$this->_title($this->__("Diva"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("diva/diva")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("diva_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("diva/diva");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Diva Manager"), Mage::helper("adminhtml")->__("Diva Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Diva Description"), Mage::helper("adminhtml")->__("Diva Description"));


		$this->_addContent($this->getLayout()->createBlock("diva/adminhtml_diva_edit"))->_addLeft($this->getLayout()->createBlock("diva/adminhtml_diva_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("diva/diva")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Diva was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setDivaData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setDivaData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("diva/diva");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('diva_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("diva/diva");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'diva.csv';
			$grid       = $this->getLayout()->createBlock('diva/adminhtml_diva_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'diva.xml';
			$grid       = $this->getLayout()->createBlock('diva/adminhtml_diva_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
