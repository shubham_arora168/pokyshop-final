<?php
class Pokyshop_Diva_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("StyleStore Rajasthan Diva 2015"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("stylestore rajasthan diva 2015", array(
                "label" => $this->__("StyleStore Rajasthan Diva 2015"),
                "title" => $this->__("StyleStore Rajasthan Diva 2015")
		   ));

      $this->renderLayout(); 
	  
    }
	
	public function postAction() {
		$post = $this->getRequest()->getPost();
		//echo '<pre>';print_r($post);die;
		if ( $post ) {
			$first_name		=	$post['first_name'];
			$middle_name	=	$post['middle_name'];
			$last_name		=	$post['last_name'];
			$dob_date		=	$post['dob_date'];
			$dob_month		=	$post['dob_month'];
			$dob_year		=	$post['dob_year'];
			$age			=	$post['age'];
			$address		=	$post['address'];
			$birth_city		=	$post['birth_city'];
			$height_feet	=	$post['height_feet'];
			$height_inches	=	$post['height_inches'];
			$weight			=	$post['weight'];
			$bust_size		=	$post['bust_size'];
			$waist_size		=	$post['waist_size'];
			$hips_size		=	$post['hips_size'];
			$mobile_number	=	$post['mobile_number'];
			$email			=	$post['email'];
			$marital_status	=	$post['marital_status'];
			$passport		=	$post['passport'];
			
		//	Image Upload
		if(isset($_FILES['close_up_img']['name']) && (file_exists($_FILES['close_up_img']['tmp_name'])) ){
			try {
				$newFileName	=	$first_name.'_'.$last_name;
				$close_up_img	=	Mage::helper('diva')->uploadImage($_FILES, 'close_up_img', $newFileName);
				$mid_shot_img	=	Mage::helper('diva')->uploadImage($_FILES, 'mid_shot_img', $newFileName);
				$full_img		=	Mage::helper('diva')->uploadImage($_FILES, 'full_img', $newFileName);
			} catch(Exception $e){
				echo $e->getMessage();	die;
			}
		}else{
			$message	=	'Please select a valid image and try again.';
			Mage::getSingleton('core/session')->addError($message);
			$this->_redirect('stylestorerajasthandiva2015');		
		}
		
		
		
		
		$insertData	= 	array(	'first_name'	=> 	$first_name,
								'middle_name' 	=> 	$middle_name,
								'last_name' 	=> 	$last_name,
								'mobile_number' => 	$mobile_number,
								'email' 		=> 	$email,
							'dob' 			=> 	$dob_date .'/'. $dob_month .'/'. $dob_year,
								'age' 			=> 	$age,
								'address' 		=> 	$address,
								'birth_city' 	=> 	$birth_city,
								'height' 		=> 	$height_feet .'.'. $height_inches,
								'weight' 		=> 	$weight,
								'bust_size' 	=> 	$bust_size,
								'waist_size' 	=> 	$waist_size,
								'hips_size' 	=> 	$hips_size,
								'married' 		=> 	$marital_status,
								'passport' 		=> 	$passport
                		);
		//prd($insertData);
		$model 		= 	Mage::getModel('diva/diva')->setData($insertData);
		try {
			$insertId = $model->save()->getId();
			//echo "Data successfully inserted. Insert ID: ".$insertId;die;
			$message	=	'Voila! Your details have been submitted successfully.';
			Mage::getSingleton('core/session')->addSuccess($message);
			$this->_redirect('/');				
		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		} catch (Exception $e) {
			$message	=	'Oops! There is some error. Please wait while we are working on it.';
			Mage::getSingleton('core/session')->addError($message);
			$this->_redirect('/');	
			Mage::log('Failure: ' . $e->getMessage(), null, 'diva.log');
			Mage::logException($e);
		}
		}
	}
}