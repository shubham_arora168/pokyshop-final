<?php
class Pokyshop_Diva_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	function uploadImage($files , $imageName, $newFileName='vaseem'){
		//pr($files);
		try {
			$uploader = new Varien_File_Uploader($imageName);	//load class
			
			$uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif'));	 //	Allowed extension for file
			$uploader->setAllowCreateFolders(true); 	//	for creating the directory if not exists
			$uploader->setAllowRenameFiles(true);		//	if true, uploaded file's name will be changed, if file with the same name already exists directory.
			$uploader->setFilesDispersion(false);
			$path 		= 	Mage::getBaseDir('media') . DS . 'diva' . DS ;	 //	desitnation directory
			
			//$destFile	=	$fileUploadPath.$files[$imageName]['name'];
			//$filename = $uploader->getNewFileName($destFile);
			
			//	rename file before upload
			$ext    		= 	explode('.', $files[$imageName]['name']);
			$ext    		= 	array_pop($ext);
			$newFilename 	= 	$imageName.'_'. $newFileName.'_'.date('d-m-Y').'.'.$ext;
			//ecd($newFilename);
			$result			=	$uploader->save( $path, strtolower($newFilename) );
			
			$uploadedFileName = $newFilename;	//	to save file name in db	
			return $uploadedFileName;
		}catch(Exception $e) {
			echo $e->getMessage();
		}
	}	
}
	 