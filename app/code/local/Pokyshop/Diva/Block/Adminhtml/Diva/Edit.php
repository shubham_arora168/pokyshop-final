<?php
	
class Pokyshop_Diva_Block_Adminhtml_Diva_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "diva_id";
				$this->_blockGroup = "diva";
				$this->_controller = "adminhtml_diva";
				$this->_updateButton("save", "label", Mage::helper("diva")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("diva")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("diva")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("diva_data") && Mage::registry("diva_data")->getId() ){

				    return Mage::helper("diva")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("diva_data")->getId()));

				} 
				else{

				     return Mage::helper("diva")->__("Add Item");

				}
		}
}