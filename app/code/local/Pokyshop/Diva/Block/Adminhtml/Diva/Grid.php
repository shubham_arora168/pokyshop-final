<?php

class Pokyshop_Diva_Block_Adminhtml_Diva_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("divaGrid");
				$this->setDefaultSort("diva_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("diva/diva")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("diva_id", array(
				"header" => Mage::helper("diva")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "diva_id",
				));
                
				$this->addColumn("first_name", array(
				"header" => Mage::helper("diva")->__("First Name"),
				"index" => "first_name",
				));
				$this->addColumn("middle_name", array(
				"header" => Mage::helper("diva")->__("Middle Name"),
				"index" => "middle_name",
				));
				$this->addColumn("last_name", array(
				"header" => Mage::helper("diva")->__("Last Name"),
				"index" => "last_name",
				));
				$this->addColumn("mobile_number", array(
				"header" => Mage::helper("diva")->__("Mobile Number"),
				"index" => "mobile_number",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("diva")->__("Email"),
				"index" => "email",
				));
					$this->addColumn('dob', array(
						'header'    => Mage::helper('diva')->__('DOB'),
						'index'     => 'dob',
						'type'      => 'datetime',
					));
				$this->addColumn("age", array(
				"header" => Mage::helper("diva")->__("Age"),
				"index" => "age",
				));
				$this->addColumn("address", array(
				"header" => Mage::helper("diva")->__("Address"),
				"index" => "address",
				));
				$this->addColumn("birth_city", array(
				"header" => Mage::helper("diva")->__("Birth City"),
				"index" => "birth_city",
				));
				$this->addColumn("height", array(
				"header" => Mage::helper("diva")->__("Height"),
				"index" => "height",
				));
				$this->addColumn("weight", array(
				"header" => Mage::helper("diva")->__("Weight"),
				"index" => "weight",
				));
				$this->addColumn("bust_size", array(
				"header" => Mage::helper("diva")->__("Bust Size"),
				"index" => "bust_size",
				));
				$this->addColumn("waist_size", array(
				"header" => Mage::helper("diva")->__("Waist Size"),
				"index" => "waist_size",
				));
				$this->addColumn("hips_size", array(
				"header" => Mage::helper("diva")->__("Hips Size"),
				"index" => "hips_size",
				));
						$this->addColumn('married', array(
						'header' => Mage::helper('diva')->__('Maarital Status'),
						'index' => 'married',
						'type' => 'options',
						'options'=>Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getOptionArray15(),				
						));
						
						$this->addColumn('passport', array(
						'header' => Mage::helper('diva')->__('Passport Details'),
						'index' => 'passport',
						'type' => 'options',
						'options'=>Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getOptionArray16(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('diva_id');
			$this->getMassactionBlock()->setFormFieldName('diva_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_diva', array(
					 'label'=> Mage::helper('diva')->__('Remove Diva'),
					 'url'  => $this->getUrl('*/adminhtml_diva/massRemove'),
					 'confirm' => Mage::helper('diva')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray15()
		{
            $data_array=array(); 
			$data_array[0]='Married';
			$data_array[1]='Unmarried';
			$data_array[2]='Divorced';
			$data_array[3]='Widow';
            return($data_array);
		}
		static public function getValueArray15()
		{
            $data_array=array();
			foreach(Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getOptionArray15() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray16()
		{
            $data_array=array(); 
			$data_array[0]='Indian Passport';
			$data_array[1]='OCI';
			$data_array[2]='NRI';
            return($data_array);
		}
		static public function getValueArray16()
		{
            $data_array=array();
			foreach(Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getOptionArray16() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}