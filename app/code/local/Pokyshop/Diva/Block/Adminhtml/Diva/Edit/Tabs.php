<?php
class Pokyshop_Diva_Block_Adminhtml_Diva_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("diva_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("diva")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("diva")->__("Item Information"),
				"title" => Mage::helper("diva")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("diva/adminhtml_diva_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
