<?php
class Pokyshop_Diva_Block_Adminhtml_Diva_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("diva_form", array("legend"=>Mage::helper("diva")->__("Item information")));

				
						$fieldset->addField("first_name", "text", array(
						"label" => Mage::helper("diva")->__("First Name"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "first_name",
						));
					
						$fieldset->addField("middle_name", "text", array(
						"label" => Mage::helper("diva")->__("Middle Name"),
						"name" => "middle_name",
						));
					
						$fieldset->addField("last_name", "text", array(
						"label" => Mage::helper("diva")->__("Last Name"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "last_name",
						));
					
						$fieldset->addField("mobile_number", "text", array(
						"label" => Mage::helper("diva")->__("Mobile Number"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "mobile_number",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("diva")->__("Email"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "email",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('dob', 'date', array(
						'label'        => Mage::helper('diva')->__('DOB'),
						'name'         => 'dob',					
						"class" => "required-entry",
						"required" => true,
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));
						$fieldset->addField("age", "text", array(
						"label" => Mage::helper("diva")->__("Age"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "age",
						));
					
						$fieldset->addField("address", "text", array(
						"label" => Mage::helper("diva")->__("Address"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "address",
						));
					
						$fieldset->addField("birth_city", "text", array(
						"label" => Mage::helper("diva")->__("Birth City"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "birth_city",
						));
					
						$fieldset->addField("height", "text", array(
						"label" => Mage::helper("diva")->__("Height"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "height",
						));
					
						$fieldset->addField("weight", "text", array(
						"label" => Mage::helper("diva")->__("Weight"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "weight",
						));
					
						$fieldset->addField("bust_size", "text", array(
						"label" => Mage::helper("diva")->__("Bust Size"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "bust_size",
						));
					
						$fieldset->addField("waist_size", "text", array(
						"label" => Mage::helper("diva")->__("Waist Size"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "waist_size",
						));
					
						$fieldset->addField("hips_size", "text", array(
						"label" => Mage::helper("diva")->__("Hips Size"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "hips_size",
						));
									
						 $fieldset->addField('married', 'select', array(
						'label'     => Mage::helper('diva')->__('Maarital Status'),
						'values'   => Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getValueArray15(),
						'name' => 'married',					
						"class" => "required-entry",
						"required" => true,
						));				
						 $fieldset->addField('passport', 'select', array(
						'label'     => Mage::helper('diva')->__('Passport Details'),
						'values'   => Pokyshop_Diva_Block_Adminhtml_Diva_Grid::getValueArray16(),
						'name' => 'passport',					
						"class" => "required-entry",
						"required" => true,
						));

				if (Mage::getSingleton("adminhtml/session")->getDivaData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getDivaData());
					Mage::getSingleton("adminhtml/session")->setDivaData(null);
				} 
				elseif(Mage::registry("diva_data")) {
				    $form->setValues(Mage::registry("diva_data")->getData());
				}
				return parent::_prepareForm();
		}
}
