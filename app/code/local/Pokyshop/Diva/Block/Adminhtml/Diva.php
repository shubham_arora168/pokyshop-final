<?php


class Pokyshop_Diva_Block_Adminhtml_Diva extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_diva";
	$this->_blockGroup = "diva";
	$this->_headerText = Mage::helper("diva")->__("Diva Manager");
	$this->_addButtonLabel = Mage::helper("diva")->__("Add New Item");
	parent::__construct();
	
	}

}