<?php

/**
 * This file is part of The Official Amazon Payments Magento Extension
 * (c) Pay with Amazon
 * All rights reserved
 *
 * Reuse or modification of this source code is not allowed
 * without written permission from Pay with Amazon
 *
 * @category   Pwa
 * @package    Pwa_PaywithAmazon
 * @copyright  Copyright (c) Pay with Amazon
 * @author     Pay with Amazon
 */
class Pwa_PaywithAmazon_Block_Badge extends Pwa_PaywithAmazon_Block_Abstract {

    public function _toHtml() {
        if ($this->_isActive()) {
            return parent::_toHtml();
        }
        return '';
    }

    public function getAmazonBadgeUrl() {

	$imageurl="pwa/images/".Mage::getStoreConfig('paywithamazon/pwaimage/image');
	if(Mage::getStoreConfig('paywithamazon/pwaimage/image'))
		return $this->getSkinUrl($imageurl);
	return $this->getSkinUrl('pwa/images/amazon-payments-badge.png');
    }

    public function getpwaimageheight(){

		$imageurl=Mage::getStoreConfig('paywithamazon/pwaimage/image');
		$imageheight=Mage::getStoreConfig('paywithamazon/pwaimage/height');
		if($imageurl && $imageheight){
			return Mage::getStoreConfig('paywithamazon/pwaimage/height');	
		}
		return '50';

   }

   public function getpwaimagewidth(){

		$imageurl = Mage::getStoreConfig('paywithamazon/pwaimage/image');
		$imagewidth = Mage::getStoreConfig('paywithamazon/pwaimage/width');
		if($imageurl && $imagewidth){
			return Mage::getStoreConfig('paywithamazon/pwaimage/width');	
		}
		return '150';

  }
	public function getpwabuttonurl(){

		$imageurl=Mage::getStoreConfig('paywithamazon/pwaimage/image');
		if($imageurl){
			$pwaimageurl="pwa/images/".Mage::getStoreConfig('paywithamazon/pwaimage/image');
			return $this->getSkinUrl($pwaimageurl);
		}
		return '';		
   	}	


}
