<?php
class Pwa_PaywithAmazon_Model_Orderstatus 
{    

    public function toOptionArray() {
       
		$orderStatusCollection = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
		$status = array();
		$status= array('-1'=>'..Please Select..');
		 
		foreach($orderStatusCollection as $orderStatus) {
		    	$status[] = array (
					'value' => $orderStatus['status'], 
					'label' => $orderStatus['label'],
					);
		}
		return $status;
    }

}
