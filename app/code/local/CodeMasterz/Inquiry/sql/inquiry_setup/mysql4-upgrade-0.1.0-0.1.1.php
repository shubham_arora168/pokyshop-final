<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	ALTER TABLE {$this->getTable('cmz_inquiry')} ADD `address` TEXT AFTER `phone_no`;
	ALTER TABLE {$this->getTable('cmz_inquiry')} ADD `pincode` INT(6) NOT NULL AFTER `address`;
");
$installer->endSetup();