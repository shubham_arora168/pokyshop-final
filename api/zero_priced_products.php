<?php
ini_set("memory_limit","10000M");
require_once "../app/Mage.php";
umask(0);
Mage::app();


$zeroPriceProducts = Mage::getModel('catalog/product')->getCollection()
    ->addAttributeToFilter(
          array(
             array('attribute'=>'price', 'eq'=>'0'),
             array('attribute'=>'price', 'isnull'=>true),
          )
    )
	->addAttributeToFilter('status',1)
	->addAttributeToFilter('visibility',4)
	;
$ids = $zeroPriceProducts->getAllIds();
if(count ($ids) > 0){
	try{
		Mage::getSingleton('catalog/product_action')->updateAttributes($ids,array('status' => 2));
		echo 'Below Products have been disabled';
		echo '<pre>';print_r($ids);
	}catch(Mage_Core_Exception $e){
		echo $e->getMessage();	
	}
}else{
	echo 'There is nothing to update.';
}
